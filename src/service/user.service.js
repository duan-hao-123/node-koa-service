// 数据库读写模块
const User = require("../model/user.model")
class UserService {
    async createUser({ user_name, password }) {
        // 写入数组库
        const res = await User.create({
            user_name,
            password
        })
        return res.dataValues
    }
    // 判断用户
    async getUserInfo({ id, user_name, password, is_admin }) {
        const whereOpt = {};
        // 当id不为空将id拷贝写入whereOpt中
        id && Object.assign(whereOpt, { id })
        user_name && Object.assign(whereOpt, { user_name })
        password && Object.assign(whereOpt, { password })
        is_admin && Object.assign(whereOpt, { is_admin })

        const ass = await User.findOne({
            attributes: ['id', 'user_name', "password", "is_admin"],
            where: whereOpt
        })
        return ass ? ass.dataValues : null
    }
    // 
    async updateBuId({ id, user_name, password, is_admin }) {
        const whereOpt = { id };
        const newUser = {}
        user_name && Object.assign(newUser, { user_name })
        password && Object.assign(newUser, { password })
        is_admin && Object.assign(newUser, { is_admin })
        console.log(newUser)
        const res = await User.update(newUser, {
            where: whereOpt
        })
        return res[0] >= 1 ? true : false
    }
}
module.exports = new UserService()