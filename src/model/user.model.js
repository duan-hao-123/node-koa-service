// DataTypes 数据类型
const { Sequelize, DataTypes } = require('sequelize');
const seq = require("../db/seq");
// 创建user表名称
const User = seq.define('User', {
    // 在这里定义模型属性
    user_name: {
        type: DataTypes.STRING,
        allowNull: false,//是否可以为空
        unique: true,
        comment: "用户名"
    },
    password: {
        type: DataTypes.CHAR(64),
        allowNull: false,
        comment: "对应密码"
    },
    is_admin: {
        type: DataTypes.BOOLEAN,
        allowNull: false,
        defaultValue: 0,//默认值
        comment: "是否为管理员"
    },

}, {
    tableName: "user_index"
});
// 同步或创建数据表 force:每次同步是否删除数据表
// User.sync({force:true})
module.exports = User
