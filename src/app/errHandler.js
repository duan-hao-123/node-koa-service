module.exports = (error, ctx) => {
    let status =  ctx.response.status?ctx.response.status:500;

    ctx.status = status;
    ctx.body = error
}