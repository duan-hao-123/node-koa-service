const Koa = require("koa")//导出框架主类
const app = new Koa();
// 处理返回内容
const KoaBody = require("koa-body")
const router = require("../router/user.route")
// 统一错误处理模块
const errorHandler = require("./errHandler")

app.use(new KoaBody())
app.use(router.routes())
// 统一错误处理
app.on('error',errorHandler)
// 支持文件上传
app.use(KoaBody({
    // 支持文件格式
    multipart: true,
    formidable: {
        // 上传目录
        uploadDir: path.join(__dirname, './static'),
        // 保留文件扩展名
        keepExtensions: true,
    }
}))
module.exports = app