const Router = require("koa-router")
const router = new Router({prefix:"/users"})
const {Registration,UserHome,Login,ModifyUser} = require("../controller/user.controller");
// 用户中间件
const {userValidator,verifyUser,encryptionPass,verifyLogin} = require("../middleware/user.middleware")
// 验证中间件
const {auth} = require("../middleware/auth.middleware")
// 用户注册
router.get("/",UserHome)
router.post("/login",userValidator,verifyLogin,Login)
// 当浏览器路由时指定是将内容转发到控制器中进行处理 当中间件验证成功后调用注册操作
router.post("/registration",userValidator,verifyUser,encryptionPass,Registration)
// 修改密码
router.patch("/changePassword",auth,encryptionPass,ModifyUser)
module.exports = router