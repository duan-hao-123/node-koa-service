const { createUser, getUserInfo, updateBuId } = require("../service/user.service");

// 导入全局变量
const { JWT_SECRET } = require("../config/config.default")
class NotepadControllar {
    // 处理不同的业务
    async UpLoad(ctx, next) {
        ctx.set('Access-Control-Allow-Origin', '*');
        ctx.set('Access-Control-Allow-Headers', 'Content-Type, Content-Length, Authorization, Accept, X-Requested-With , yourHeaderFeild');
        ctx.set('Access-Control-Allow-Methods', 'PUT, POST, GET, DELETE, OPTIONS');

    }
}
module.exports = new UserControllar