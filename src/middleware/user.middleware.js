// 中间件内容
const { getUserInfo } = require("../service/user.service");
// 错误类型弹出
const { userFormateError,userPasswordError, userAlreadyExited,userLoginError, userDoesNotExist, userRegisterError } = require("../constant/err.type.js");
// node加密库
const bcrypt = require("bcryptjs")
// 判断内容是否为空
const userValidator = async (ctx, next) => {
    let { body } = ctx.request
    // 加入验证
    if (!body.user_name && !body.password) {
        // 储藏错误日志
        console.error('用户名或密码为空', ctx.body)
        ctx.app.emit('error', userFormateError, ctx)
        return false
    }
    await next()
}
// 判断合理性
const verifyUser = async (ctx, next) => {
    let { body } = ctx.request;
    // 合理性
    try {
        const res = await getUserInfo({ user_name: body.user_name })
        if (res) {
            console.error('用户名称已存在', body.user_name)
            ctx.app.emit('error', userAlreadyExited, ctx)
            return
        }
    } catch (err) {
        console.error('获取用户信息错误', err)
        ctx.app.emit('error', userRegisterError, ctx)
    }
    await next()
}
// 密码修改
const encryptionPass = async (ctx, next) => {
    // 提取密码
    const { password } = ctx.request.body;
    // 加密十次
    const salt = bcrypt.genSaltSync(10)
    const hash = bcrypt.hashSync(password, salt)
    ctx.request.body.password = hash;
    await next()
}
// 验证登录
const verifyLogin = async (ctx, next) => {
    let { user_name, password } = ctx.request.body;
    let res = {};
    // 查找用户是否存在 获取用户信息失败后返回状态
    try {
        res = await getUserInfo({ user_name })
        if (!res) {
            console.error("登录用户不存在")
            ctx.app.emit('error', userDoesNotExist, ctx)
            return
        }
    } catch(err) {
        console.error(err)
        ctx.app.emit('error', userLoginError, ctx)
    }
    // 判断密码是否匹配
    if(!bcrypt.compareSync(password,res.password)){
        ctx.app.emit("error",userPasswordError,ctx)
        return
    }

    await next()
}
module.exports = {
    userValidator,
    verifyUser,
    encryptionPass,
    verifyLogin
}