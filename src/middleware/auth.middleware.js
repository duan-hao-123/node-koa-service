// 错误信息
const {tokenExpiredError,jsonWebTokenError} = require("../constant/err.type")
// node加密库
const jwt = require("jsonwebtoken");
//导入私钥
const { JWT_SECRET } = require("../config/config.default")
//验证token
const auth = async (ctx, next) => {
    let { authorization } = ctx.request.header;
    try {
        // user中包含payload信息
        const user = jwt.verify(authorization, JWT_SECRET);
        ctx.state.user = user

    } catch (err) {
        switch(err.name){
            case 'TokenExpiredError':
                console.error("token已经过期")
                ctx.app.emit("error",tokenExpiredError,ctx)
                return false
            case 'JsonWebTokenError':
                console.error("token验证失败")
                ctx.app.emit("error",jsonWebTokenError,ctx)
                return false
        }
    }
    await next()
}

module.exports = {
    auth
}