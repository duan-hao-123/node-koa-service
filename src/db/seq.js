const {MYSQL_HOST,
    MYSQL_POST,
    MYSQL_USER,
    MYSQL_PWD,
    MYSQL_DB
} = require("../config/config.default")
// 引入mysql数据库插件sequelize
const {Sequelize} = require("sequelize");
// 实例化对象
const seq = new Sequelize(MYSQL_DB,MYSQL_USER, MYSQL_PWD,{
    host:MYSQL_HOST,
    dialect:"mysql"
})
// 测试是否连接
seq.authenticate()
    .then(()=>{
        console.log("数据库连接成功")
    })
    .catch(()=>{
        console.log("数据库连接失败")
    })
module.exports = seq